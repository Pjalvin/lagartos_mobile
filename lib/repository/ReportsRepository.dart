import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:geolocator/geolocator.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidebar_animation/model/Caza.dart';
import 'package:sidebar_animation/model/Cupo.dart';
import 'package:http/http.dart' as http;
import 'package:sqflite/sqflite.dart';
import '../core/apiUrl.dart' as api;
import '../libs/verifyConnection.dart'as verify;
import '../libs/Toast.dart'as Toast;
import 'package:path/path.dart';
import './GeoRepository.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
class ReportsRepository{
  Future  obtenerReportesCaza()async {
    bool ver = await verify.verify();
    List<Caza> cazaList = List();
    Cupo cupo=Cupo();
    if (ver) {
      try {
        SharedPreferences sha = await SharedPreferences.getInstance();
        String token = sha.getString("token");
        final response = await http.get(api.url + "/caza/",
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
              'authorizationApp': token,
              'authorization': env["API_TOKEN"]
            });
        if (response.statusCode == 200) {
          var cazas = jsonDecode(response.body);
          cupo.totalCupo=cazas["cupoTotal"];
          cupo.totalUsado=cazas["cupoUsado"];
          for (var caza in cazas["cazas"]) {
            var newCaza = Caza(itemId: caza["itemId"],
                estado: caza["estado"],
                fechaReporte: caza["fecha_reporte"],
                coorX: (caza["coor_x"]) * 1.0,
                coorY: (caza["coor_y"]) * 1.0,
                cantidad: caza["cantidad"]);
            cazaList.add(newCaza);
          }
        }
        else {
        }
      }
      catch (e) {
      }
    }
    return [cazaList,cupo];

  }
  Future<List<Caza>> reportesLocal() async{
    List<Caza> cazaListLocal=List();
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'otca.db');
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute(
              'CREATE TABLE IF NOT EXISTS caza (itemId INTEGER PRIMARY KEY autoincrement, cantidad INTEGER, coor_x REAL, coor_y REAL)');
        });
    List<Map> lisCaza = await database.rawQuery("SELECT * FROM caza");
    lisCaza.forEach((element) {
      Caza newCazaLocal = Caza();
      newCazaLocal.cantidad = element["cantidad"];
      newCazaLocal.coorY = element["coor_y"];
      newCazaLocal.coorX = element["coor_x"];
      cazaListLocal.add(newCazaLocal);
    });
    return cazaListLocal;
  }
  Future<bool> obtenerReportePDF(id)async {
    bool ver=await verify.verify();
    if(ver) {
      try {
        SharedPreferences sha = await SharedPreferences.getInstance();
        String _localPath = (await _findLocalPath()) + Platform.pathSeparator +
            'Download';
        if (await _checkPermission()) {
          File file=new File(_localPath+"/caza"+id.toString()+".pdf");
          var exist=await file.exists();
          if(!exist){

          SharedPreferences sha = await SharedPreferences.getInstance();
          String token = sha.getString("token");
          final response = await http.get(api.url + "/caza/pdf/" + id.toString(),
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
                'authorizationApp': token,
                'authorization': env["API_TOKEN"]
              });
          if (response.statusCode == 200) {
            await file.writeAsBytes(response.bodyBytes);
            OpenFile.open(_localPath+"/caza"+id.toString()+".pdf");
            return true;
          }
          else {
            return false;
          }}
          else{
            OpenFile.open(_localPath+"/caza"+id.toString()+".pdf");

          }
        }
        else {
          return false;
        }
      }
      catch (e) {
        return false;
      }
    }
    else{
      Toast.toastError("Sin conexión");
      return false;
    }
    }
  Future<int> subirReporteCaza(Caza caza)async {
    bool ver=await verify.verify();
    if(ver==null){
      Toast.toastOk("No se pudo obtener la ubicación.");
      return 0;
    }
    else if(ver) {
      try {
        SharedPreferences sha = await SharedPreferences.getInstance();
        String token = sha.getString("token");
        if(token!=null){
          final response = await http.post(api.url + "/caza/",
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
                'authorizationApp': token,
                'authorization': env["API_TOKEN"]
              },
              body: jsonEncode(caza.toJson()));
          if (response.statusCode == 200) {
            return 1;
          }
          else {
            Toast.toastError("No se pudo subir el reporte");
            return 0;
          }
        }
        else{
          return 1;
        }
      }
      catch (e) {
        Toast.toastError("No se pudo subir el reporte");
        return 0;
      }
    }
    else{
      try{
        Position pos=await GeoRepository().obtenerUbicacion();
        if(pos!=null){
          caza.coorY=pos.longitude;
          caza.coorX=pos.latitude;
          Toast.toastOk("El reporte se subira localmente.");
          var databasesPath = await getDatabasesPath();
          String path = join(databasesPath, 'otca.db');
          Database database = await openDatabase(path, version: 1,
              onCreate: (Database db, int version) async {
                await db.execute(
                    'CREATE TABLE if not exist caza (itemId INTEGER PRIMARY KEY autoincrement, cantidad INTEGER, coor_x REAL, coor_y REAL)');
              });
          await database.rawInsert('INSERT INTO caza VALUES(null, ?, ? , ? )',[caza.cantidad,caza.coorX,caza.coorY]);
          return 2;
        }
        else{
          Toast.toastOk("No se pudo obtener la ubicación.");
          return 0;
        }
      }
      catch(e){
        return 0;
      }
    }

  }
  Future<int> subirReporteCazaLocal(List<Caza> cazas)async {
    bool ver=await verify.verify();
    if(ver) {
      SharedPreferences sha = await SharedPreferences.getInstance();
    String token = sha.getString("token");
     if(token!=null){
       try {
         List cazasAux=List();
         cazas.forEach((element) {
           cazasAux.add(element.toJson());
         });
         final response = await http.post(api.url + "/caza/multiple",
             headers: <String, String>{
               'Content-Type': 'application/json; charset=UTF-8',
               'authorizationApp': token,
               'authorization': env["API_TOKEN"]
             },
             body: json.encode({"cazas":cazasAux}));
         if (response.statusCode == 200) {
           var databasesPath = await getDatabasesPath();
           String path = join(databasesPath, 'otca.db');
           Database database = await openDatabase(path, version: 1,
               onCreate: (Database db, int version) async {
                 await db.execute(
                     'CREATE TABLE if not exist caza (itemId INTEGER PRIMARY KEY autoincrement, cantidad INTEGER, coor_x REAL, coor_y REAL)');
               });
           await database.rawDelete('DELETE FROM caza');
           return 1;
         }
         else {
           return 0;
         }
       }
       catch (e) {
         return 0;
       }
     }
       else{
         return 2;
     }
    }else{
      return 0;
    }

  }
  Future<String> _findLocalPath() async {
    final directory = await getExternalStorageDirectory();
    return directory.parent.parent.parent.parent.path;
  }
  Future<bool> _checkPermission() async {
      final status = await Permission.storage.status;
      if (status != PermissionStatus.granted) {
        final result = await Permission.storage.request();
        if (result == PermissionStatus.granted) {
          return true;
        }
      } else {
        return true;
      }

  }






}