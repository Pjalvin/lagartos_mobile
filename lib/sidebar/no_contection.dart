import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/loginBloc.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/noConectionBloc.dart';
import 'package:sidebar_animation/events/loginEvent.dart';
import 'package:sidebar_animation/pages/loadingPage.dart';
import 'package:sidebar_animation/pages/reporte.dart';
import 'package:sidebar_animation/repository/ReportsRepository.dart';
import 'package:sidebar_animation/states/noConectionState.dart';

class NoConectionWidget extends StatefulWidget {
  @override
  _NoConectionState createState() => _NoConectionState();
}

class _NoConectionState extends State<NoConectionWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<NoConectionBloc>(
        create: (context) => NoConectionBloc(ReportsRepository()),
        child:Stack(
          children: <Widget>[
            BlocBuilder<NoConectionBloc, NoConectionState>(
              builder: (context, noConectionState) {

                    if(noConectionState is NoConectionReporte){

                       return Reporte(null);
                    }
                    else if(noConectionState is NoConectionLoading){
                      return LoadingPage("Reporte","assets/images/imgreporte2.jpg");
                    }
                    else if(noConectionState is NoConectionReporteOnline){
                      BlocProvider.of<LoginBloc>(context).add(CerrarLogin());
                      return Container();
                    }
                    else return Container();

              },
            ),
          ],
        ),

      ),
    );
  }
}
