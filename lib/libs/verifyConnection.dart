

import 'dart:io';

import 'package:geolocator/geolocator.dart';

verify()async {
    try{
      var result=await InternetAddress.lookup("google.com");
      Position position = await getCurrentPosition(timeLimit: Duration(seconds: 120),);
      if(position==null){
        return null;
      }
      else if(result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    on SocketException catch (_) {
      Position position = await getCurrentPosition(timeLimit: Duration(seconds: 120),);
      if(position!=null){
        return false;
      }
    }
    catch(e){
      return null;
    }
}