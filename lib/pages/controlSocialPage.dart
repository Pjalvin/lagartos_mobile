import 'package:flutter/material.dart';
import 'package:sidebar_animation/core/color.dart';
import 'package:sidebar_animation/pages/informePage.dart';
import 'package:sidebar_animation/repository/EstadoTco.dart';
import '../libs/Toast.dart' as Toast;
class ControlSocialPage extends StatefulWidget {
  @override
  _ControlSocialPageState createState() => _ControlSocialPageState();
}

class _ControlSocialPageState extends State<ControlSocialPage> {
  Size size;
  @override
  Widget build(BuildContext context) {
    size=MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: size.height*0.35,
                  width: size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/hojas3.jpg"),
                        fit: BoxFit.cover
                    ),
                  ),
                ),
                Positioned(
                  right: 30,
                  top: (size.height*0.15),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Control Social',
                        style: TextStyle(color:Colors.white, fontSize:size.height*0.05 ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>InformePage()));
                      },
                      child: Container(
                        height: 50,
                        width: size.width*0.80,
                        decoration: BoxDecoration(
                          color: color2,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.4),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0,3),
                            ),
                          ],
                        ),
                        child: Center(child: Text("Página de información",style: TextStyle(fontSize:size.width*0.05,color: color1,fontWeight: FontWeight.bold),)),
                      ),
                    ),
                    GestureDetector(
                      onTap: ()async {
                        EstadoTcoRepository tco=EstadoTcoRepository();
                        Toast.toastIniciarDescarga();
                        bool res=await tco.obtenerResolucion();
                        if(!res){
                          Toast.toastError("No se pudo realizar la descarga");
                        }
                      },
                      child: Container(
                        height: 50,
                        width: size.width*0.80,
                        decoration: BoxDecoration(
                          color: color2,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.4),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0,3),
                            ),
                          ],
                        ),
                        child: Center(child: Text("Descargar Resolución",style: TextStyle(fontSize:size.width*0.05,color:  color1,fontWeight: FontWeight.bold),)),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 50,
                        width: size.width*0.80,
                        decoration: BoxDecoration(
                          color: color1,
                          borderRadius: BorderRadius.circular(30.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.4),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0,3),
                            ),
                          ],
                        ),
                        child: Center(child: Text("Volver",style: TextStyle(fontSize:size.width*0.05,color: Colors.white,fontWeight: FontWeight.bold),)),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
