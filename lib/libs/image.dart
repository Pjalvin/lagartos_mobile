import 'dart:math';
import 'dart:core';
generateId(){
  const chars =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var autoId = '';
  for (var i = 0; i < 20; i++) {
    var n=Random().nextDouble()* chars.length;
  autoId += chars.substring(1,n.floor());
  }
  return autoId;
}