class Cupo{
  int _totalCupo;
  int _totalUsado;

  Cupo();

  int get totalUsado => _totalUsado;

  set totalUsado(int value) {
    _totalUsado = value;
  }

  int get totalCupo => _totalCupo;

  set totalCupo(int value) {
    _totalCupo = value;
  }
}