import 'package:bloc/bloc.dart';
import 'package:sidebar_animation/events/loginEvent.dart';
import 'package:sidebar_animation/repository/LoginRepository.dart';
import 'package:sidebar_animation/states/loginState.dart';
import '../libs/Toast.dart' as Toast;



class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginRepository loginRepository;
  LoginBloc(this.loginRepository);
  @override
  // TODO: implement initialState
  LoginState get initialState => LoginInit();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if(event is LoginClick){
      yield LoginLoading();
      var login= await loginRepository.iniciarSesion(event.login);
      if(login){
        yield LoginOk();
      }
      else{
        Toast.toastError("Error en los datos ingresados");
        yield LoginFailed();
      }
    }
    else if(event is VerificarLogin){
      int login= await loginRepository.obtenerPerfil();
      switch(login){
        case 1:
          yield LoginOk();
          break;
        case 2:
          yield NoConection();
          break;
        case 3:
          yield NoGPS();
          break;
        case 0:
          yield LoginFailed();
          break;
      }
    }
    else if(event is CerrarLogin){
      yield LoginInit();
      var login= await loginRepository.cerrarSesion();
      if(login){
        yield LoginInit();
      }
      else{
        yield LoginInit();
      }
    }
  }

}
