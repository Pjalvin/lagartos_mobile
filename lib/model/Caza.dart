
class Caza{
  int _itemId;
  int _estado;
  int _cantidad;
  String _fechaReporte;
  double _coorX;
  double _coorY;

  Caza({itemId, estado, fechaReporte, coorX, coorY,cantidad,imagenes}):_itemId = itemId,_estado=estado,_fechaReporte=fechaReporte,_coorX=coorX,_coorY=coorY,_cantidad=cantidad;



  double get coorY => _coorY;

  int get cantidad => _cantidad;

  set cantidad(int value) {
    _cantidad = value;
  }

  set coorY(double value) {
    _coorY = value;
  }

  double get coorX => _coorX;

  set coorX(double value) {
    _coorX = value;
  }

  String get fechaReporte => _fechaReporte;

  set fechaReporte(String value) {
    _fechaReporte = value;
  }

  int get estado => _estado;

  set estado(int value) {
    _estado = value;
  }

  int get itemId => _itemId;

  set itemId(int value) {
    _itemId = value;
  }
  Map toJson() => {
    'cantidad': cantidad,
    'coorX': coorX,
    'coorY': coorY,
  };
}