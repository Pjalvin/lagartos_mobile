import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/navigation_bloc.dart';
import 'package:sidebar_animation/events/NavigationEvent.dart';
import 'package:sidebar_animation/events/loginEvent.dart';
import 'package:sidebar_animation/model/UserLogin.dart';
import 'package:sidebar_animation/pages/controlSocialPage.dart';
import 'package:sidebar_animation/pages/informePage.dart';
import '../bloc.navigation_bloc/loginBloc.dart';
import '../core/color.dart';

// class Login extends StatelessWidget with NavigationStates {
//   @override
//   Widget build(BuildContext context) {
//     Size size=MediaQuery.of(context).size;
//     return Scaffold(
//       backgroundColor: Color(0xFF225957),
//       body: SingleChildScrollView(
//         //controller: controller,
//         child: Column(
//           children: <Widget>[
//             SizedBox(
//               height: size.height,
//               child: Stack(
//                 children: <Widget>[
//                   Container(
//                     height: size.height/3,
//                     width: size.height,
//                     decoration: BoxDecoration(
//                       image: DecorationImage(
//                           image: AssetImage("assets/images/imghojas.jpg"),
//                           fit: BoxFit.fill
//                       ),
//                     ),
//                   ),
//                   Container(
//                     margin: EdgeInsets.only(top: (size.height*0.4)-70),
//                     height: size.height,
//                     decoration: BoxDecoration(
//                       color: Colors.white,
//                       borderRadius: BorderRadius.only(
//                         topLeft:Radius.circular(40),
//                         topRight:Radius.circular(40),
//                       ),
//                     ),
//
//                   ),
//                   Positioned(
//                     right: 30,
//                     top: (size.height*0.4)/2-20,
//                     child: Column(
//                       children: <Widget>[
//                         Text(
//                           'Historial',
//                           style: TextStyle(color:Colors.white, fontSize:40 ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   Positioned(
//
//                     child: Container(
//                       width: size.height*0.8,
//                       decoration: BoxDecoration(
//                         color: Color(0xFF225957),
//                         borderRadius: BorderRadius.circular(10),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
class Scroll extends ScrollBehavior{

}
class Login extends StatelessWidget  {
  bool loading=false;
  Login(this.loading);
  Size size;
  TextEditingController usuario=TextEditingController();
  TextEditingController password=TextEditingController();
  @override
  Widget build(BuildContext context) {
    size=MediaQuery.of(context).size;
    return Stack(
      children: [
        Scaffold(
          backgroundColor: color1,
          body: ScrollConfiguration(
            behavior: Scroll(),
            child: GlowingOverscrollIndicator(
              axisDirection: AxisDirection.down,
              color: Colors.transparent,
              child: SingleChildScrollView(

                //controller: controller,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: size.height,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: size.height*0.5,
                            width: size.height,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.darken),
                                image: AssetImage("assets/images/loginfondo.png"),
                                fit: BoxFit.cover,

                              ),
                            ),
                          ),
                          Positioned(
                            top:size.height*0.05,
                            right:0,
                            child:Container(
                              height: size.height*0.05,
                              color: color2.withOpacity(0.3),
                              // width: size.width*0.8,
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.end,
                                children:<Widget>[
                                  Image(
                                    image:AssetImage(
                                        "assets/images/proyecto_amazonia.png"
                                    ),
                                  ),
                                  Image(
                                    image:AssetImage(
                                        "assets/images/otca.png"
                                    ),
                                  ),
                                  Image(
                                    image:AssetImage(
                                        "assets/images/german.png"
                                    ),
                                  ),
                                  Image(
                                    image:AssetImage(
                                        "assets/images/kfw.png"
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: size.width,
                            margin: EdgeInsets.only(top: (size.height*0.35)),
                            height: size.height,
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.4),
                              borderRadius: BorderRadius.only(
                                topLeft:Radius.circular(40),
                                topRight:Radius.circular(40),
                              ),
                            ),
                            child:Column(
                              children: <Widget>[
                                Container(
                                    height: size.height*0.15,
                                    child:Center(
                                      child: Image(
                                        image:AssetImage(
                                            "assets/images/logo_mmaya_white.png"
                                        ),
                                        width: size.width*0.6,
                                      ),
                                    )
                                )
                              ],
                            ),

                          ),
                          Container(
                            margin: EdgeInsets.only(top: (size.height*0.5-2)),
                            padding: EdgeInsets.only(left:size.width*0.12,right:size.width*0.12),
                            height: size.height,
                            width: size.width,
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.9),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: size.width,
                                  child:Text(
                                    'Nombre de Usuario',
                                    style: TextStyle(color:color1, fontSize:20 ),
                                  ),
                                ),
                                Divider(
                                  height: 5,
                                  color: Colors.transparent,
                                ),
                                Container(
                                  height: 45,
                                  child:TextFieldContainer(
                                    child: TextField(
                                      controller: usuario,
                                      cursorColor: Colors.white,
                                      cursorWidth: 1,


                                      style: TextStyle(color: Colors.white,fontSize: 18),
                                      decoration: InputDecoration(

                                        fillColor: Colors.white,
                                        icon: Icon(Icons.person,color: Colors.white,),
                                        focusColor: Colors.transparent,
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(color: Colors.white.withOpacity(0.3)),
                                        // focusColor: Colors.white,
                                        // labelStyle: TextStyle(color: Colors.white),
                                      ),
                                      textInputAction: TextInputAction.next,
                                    ),
                                  ),
                                ),
                                Divider(
                                  height: 30,
                                  color: Colors.transparent,
                                ),

                                Container(
                                  width: size.width,
                                  child:Text(
                                    'Contraseña',
                                    style: TextStyle(color:color1, fontSize:20 ),
                                  ),
                                ),
                                Divider(
                                  height: 5,
                                  color: Colors.transparent,
                                ),
                                Container(
                                  height: 45,
                                  child: TextFieldContainer(
                                    child: TextField(
                                      controller: password,
                                      cursorColor: Colors.white,
                                      cursorWidth: 1,
                                      obscureText: true,
                                      style: TextStyle(color: Colors.white,fontSize: 18),
                                      decoration: InputDecoration(
                                        fillColor: Colors.white,
                                        icon: Icon(Icons.vpn_key,color: Colors.white,),
                                        border: InputBorder.none,

                                        hintStyle: TextStyle(color: Colors.white.withOpacity(0.3)),
                                        // focusColor: Colors.white,
                                        // labelStyle: TextStyle(color: Colors.white),
                                      ),
                                      textInputAction: TextInputAction.done,
                                    ),
                                  ),
                                ),
                                Divider(
                                  height: 40,
                                  color: Colors.transparent,
                                ),
                                RaisedButton(
                                    color: Color(0xFFEEEBD3),
                                    child: Text(
                                      'Iniciar Sesión',
                                      style: TextStyle(color:color1, fontSize:20 ),
                                    ),
                                    onPressed: (){
                                      if(usuario.text=="" || password.text==""){
                                        Fluttertoast.showToast(
                                            msg: "Llene todos los campos",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: color5.withOpacity(0.7),
                                            textColor: Colors.white,
                                            fontSize: 16.0
                                        );
                                      }else{
                                        userLogin user=userLogin(usuario.text,password.text);
                                        BlocProvider.of<LoginBloc>(context).add(LoginClick(user));
                                      }
                                      // if(usuario!=null && password!=null){
                                      //   userLogin user=userLogin(usuario.text,password.text);
                                      //   BlocProvider.of<LoginBloc>(context).add(LoginClick(user));
                                      // }else{
                                      //   Fluttertoast.showToast(
                                      //             msg: "Llene todos los campos",
                                      //             toastLength: Toast.LENGTH_SHORT,
                                      //             gravity: ToastGravity.CENTER,
                                      //             timeInSecForIosWeb: 1,
                                      //             backgroundColor: Colors.red,
                                      //             textColor: Colors.white,
                                      //             fontSize: 16.0
                                      //         );
                                      // }
                                    }
                                ),
                              ],
                            ),

                          ),
                          Positioned(
                              right: (size.height*0.4)/20,
                              bottom: (size.height*0.4)/20,
                              child: IconButton(
                                  icon: Icon(Icons.info_outline,color: color1,size: 30,),
                                  onPressed: (){
                                    // onTap(){
                                    //   BlocProvider.of<NavigationBloc>(context).add(informePageClickedEvent());
                                    // }
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>ControlSocialPage()));
                                    // BlocProvider<NavigationBloc>(create: (context) => informePageClickedEvent());
                                  }
                              )
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ),
        loading?Container(
              height: size.height,
              width: size.width,
              color:color5.withOpacity(0.5),
              child: Center(
                  child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator(
                      backgroundColor: color1.withOpacity(0),

                      valueColor: new AlwaysStoppedAnimation<Color>(color2),
                    ),
                  )
              )
          ):Container(),
      ],
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
}): super(key:key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(10),
      height: (size.height*0.4)/6,
      width: (size.height*0.4)*0.99,
      decoration: BoxDecoration(
        color: color1,
        // color: Colors.black,
        borderRadius: BorderRadius.circular(8),
      ),
      child: child,
    );

  }
}