class Tco{
  int _itemId;
  String _tco;
  int _municipioItemId;
  Tco();

  String get tco => _tco;

  set tco(String value) {
    _tco = value;
  }

  int get itemId => _itemId;

  set itemId(int value) {
    _itemId = value;
  }

  int get municipioItemId => _municipioItemId;

  set municipioItemId(int value) {
    _municipioItemId = value;
  }

  Map toJson() => {
    'itemId': itemId,
    'tco': tco,
    'municipio_itemId': municipioItemId,
  };
}