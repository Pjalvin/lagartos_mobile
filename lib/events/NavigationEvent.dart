
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:sidebar_animation/model/Caza.dart';

abstract class NavigationEvent extends Equatable {}

class InitEvent extends NavigationEvent {
  @override
  List<Object> get props => [];
}
class HistorialClickedEvent extends NavigationEvent {
  @override
  List<Object> get props => [];
}
class ReporteClickedEvent extends NavigationEvent{
  @override
  List<Object> get props => [];
}
class HomePageClickedEvent extends NavigationEvent {
  @override
  List<Object> get props => [];
}
class reporteClickedEvent extends NavigationEvent {
  Caza caza;
  reporteClickedEvent(this.caza);
  @override
  List<Object> get props => [caza];
}
class reportePDFEvent extends NavigationEvent{
  int id;
  reportePDFEvent(this.id);
  @override
  List<Object> get props => [id];

}
class informePageClickedEvent extends NavigationEvent{
  @override
  List<Object> get props => [];
}
class ReportesLocalClickedEvent extends NavigationEvent{
  List<Caza> cazas;
  BuildContext context;
  ReportesLocalClickedEvent(this.cazas,this.context);
  @override
  List<Object> get props => [cazas,context];
}

