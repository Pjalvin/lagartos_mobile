class EstadoTco{
  int _tenencia;
  int _entregado;
  double _vendido;

  EstadoTco();

  double get vendido => _vendido;

  set vendido(double value) {
    _vendido = value;
  }

  int get entregado => _entregado;

  set entregado(int value) {
    _entregado = value;
  }

  int get tenencia => _tenencia;

  set tenencia(int value) {
    _tenencia = value;
  }
}