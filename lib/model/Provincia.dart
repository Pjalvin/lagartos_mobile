class Provincia{
  int _itemId;
  String _nombreProvincia;
  String _departamentoItemId;
  Provincia();

  String get nombreProvincia => _nombreProvincia;

  set nombreProvincia(String value) {
    _nombreProvincia = value;
  }

  int get itemId => _itemId;

  set itemId(int value) {
    _itemId = value;
  }

  String get departamentoItemId => _departamentoItemId;

  set departamentoItemId(String value) {
    _departamentoItemId = value;
  }

  Map toJson() => {
    'itemId': itemId,
    'nombre_provincia': nombreProvincia,
    'departamentoItemId':departamentoItemId
  };
}