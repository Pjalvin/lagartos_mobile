import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/loginBloc.dart';
import 'package:sidebar_animation/libs/Toast.dart';
import 'package:sidebar_animation/loading.dart';
import 'package:sidebar_animation/pages/login.dart';
import 'package:sidebar_animation/repository/LoginRepository.dart';
import 'package:sidebar_animation/sidebar/no_contection.dart';
import 'package:sidebar_animation/states/loginState.dart';

import 'sidebar/sidebar_layout.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
void main() async{

  WidgetsFlutterBinding.ensureInitialized();
  await DotEnv.load(fileName: ".env");

  /*ByteData data = await rootBundle.load('assets/raw/cert.crt');
  SecurityContext context = SecurityContext.defaultContext;
  context.setTrustedCertificatesBytes(data.buffer.asUint8List());*/
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());}
class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        primaryColor: Colors.white
      ),
      home: BlocProvider(
        create:(context2)=>LoginBloc(LoginRepository()),
        child: BlocBuilder<LoginBloc,LoginState>(
          builder: (context,stateLogin) {
            if(stateLogin is LoginLoading){
              return Login(true);
            }
            else if(stateLogin is LoginOk){
              return  SideBarLayout();
            }
            else if(stateLogin is LoginInit){
              return Loading();
            }
            else if(stateLogin is NoConection){
              return NoConectionWidget();
            }
            else if (stateLogin is NoGPS){
              toastError("Habilite los servicios de ubicación");
              return Loading();
            }
            else{
              return Login(false);
            }


          },
        ),
      )
    );
  }
}
