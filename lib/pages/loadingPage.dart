//Esta es mi pantalla de carga donde verifica el login

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/navigation_bloc.dart';
import '../core/color.dart';


class LoadingPage extends StatelessWidget with NavigationStates {

  String titulo;
  String img;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      backgroundColor: Color(0xFF225957),
      body: SingleChildScrollView(
        //controller: controller,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: size.height,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: size.height/3,
                    width: size.height,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(img),
                          fit: BoxFit.fill
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: (size.height*0.4)-70),
                    height: size.height,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft:Radius.circular(40),
                        topRight:Radius.circular(40),
                      ),
                    ),


                  ),
                  Positioned(
                    right: 30,
                    top: (size.height*0.4)/2-20,
                    child: Column(
                      children: <Widget>[
                        Text(
                          titulo,
                          style: TextStyle(color:Colors.white, fontSize:40 ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    color: color5.withOpacity(0.5),
                    child:  Center(
                      child: Container(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.transparent,
                          valueColor: new AlwaysStoppedAnimation<Color>(color2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

  }

  LoadingPage(this.titulo,this.img);
}