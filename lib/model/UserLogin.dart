class userLogin{
  String _usuario;
  String _password;

  userLogin(this._usuario, this._password);

  String get password => _password;

  String get usuario => _usuario;

  set password(String value) {
    _password = value;
  }

  String get userName => _usuario;

  set userName(String value) {
    _usuario = value;
  }

  Map toJson() => {
    'usuario': usuario,
    'password': password,
  };
}