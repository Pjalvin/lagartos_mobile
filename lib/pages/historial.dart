import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sidebar_animation/events/NavigationEvent.dart';
import 'package:sidebar_animation/libs/MyBehavior.dart';
import 'package:sidebar_animation/core/color.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';

class Historial extends StatelessWidget with NavigationStates {
  Size size;
  List cazas=List();
  Historial(this.cazas);

  AutoSizeGroup autoSizeGroup=AutoSizeGroup();
  @override
  Widget build(BuildContext context) {
    size=MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFF225957),
      body: SingleChildScrollView(
      //controller: controller,
      child: Column(
        children: <Widget>[
            SizedBox(
              height: size.height,
                child: Stack(
                children: <Widget>[
                  Container(
                    height: size.height/2.9,
                    width: size.height,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/imghojas.jpg"),
                          fit: BoxFit.fill
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: EdgeInsets.only(top: (size.height*0.275)),
                    height: size.height*0.07,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.8),
                      borderRadius: BorderRadius.only(
                        topLeft:Radius.circular(40),
                        topRight:Radius.circular(40),
                      ),
                    ),
                    child:Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(width: size.width*0.1,child: Center(child: AutoSizeText("  N°",maxLines:1,group: autoSizeGroup,textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: 18),))),
                              Container(width: size.width*0.4,child: Center(child: AutoSizeText(" Fecha ",maxLines:1,group: autoSizeGroup,textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: 18),))),
                              Container(width: size.width*0.2,child: Center(child: AutoSizeText(" Cantidad",maxLines:1,group: autoSizeGroup,textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: 18),))),
                              Container(width: size.width*0.3,child: Center(child: AutoSizeText(" Acciones ",maxLines:1,group: autoSizeGroup,textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: 18),))),
                            ],
                          ),
                        ],
                      ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: (size.height*0.26+(size.height/12))),
                    height: size.height,
                    width: size.width,
                    decoration: BoxDecoration(
                      color: color2,
                    ),
                    child: ScrollConfiguration(
                      behavior: MyBehavior(),
                      child: ListView.builder(
                        padding: EdgeInsets.all(0),
                        itemBuilder: (context,index){
                          return Container(
                            width: size.width,
                            height: size.height*0.07,
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.8)
                            ),
                            child:Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(width: size.width*0.1,child: Center(child: AutoSizeText((index+1).toString(),textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: 18),maxLines: 1,))),
                                    Container(width: size.width*0.4,child: Center(child: AutoSizeText(cazas[0][index].fechaReporte.substring(0,10),textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: 18),maxLines: 1,))),
                                    Container(width: size.width*0.2,child: Center(child: AutoSizeText(cazas[0][index].cantidad.toString(),textAlign: TextAlign.center,style: TextStyle(color:Colors.green,fontSize: 18),maxLines: 1,))),
                                    GestureDetector(
                                      onTap: (){
                                        BlocProvider.of<NavigationBloc>(context).add(reportePDFEvent(cazas[0][index].itemId));
                                      },
                                      child: Container(width: size.width*0.3,child: Center(child: Text("Descargar",textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: 18),))),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                        itemCount: cazas[0].length,
                      ),
                    )
                  ),
                  // Container(
                  //   margin: EdgeInsets.only(top: (size.height*0.5-2)),
                  //   padding: EdgeInsets.only(left:size.width*0.12,right:size.width*0.12),
                  //   height: size.height,
                  //   width: size.width,
                  //   decoration: BoxDecoration(
                  //     color: Colors.white.withOpacity(0.9),
                  //   ),
                  // ),
                  Positioned(
                    right: 30,
                    top: (size.height*0.08),
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Historial',
                          style: TextStyle(color:Colors.white, fontSize:40 ),
                          ),
                      ],
                    ),
                  ),
                  cazas[1].totalCupo!=null?Positioned(
                      top: (size.height*0.17),
                      right: 30,
                      child: Container(
                        height: size.height*0.06,
                        // color: Colors.yellow,
                        child: Row(
                          children: [
                            Text("Total cazado: ", style: TextStyle(color: color2,fontSize: size.height*0.04),),
                            SizedBox(width: size.width*0.01,),
                            Container(
                              // color: Colors.black.withOpacity(0.7),
                              width: size.width*0.3,
                              decoration: BoxDecoration(
                                color: (cazas[1].totalUsado>cazas[1].totalCupo?Colors.red:cazas[1].totalUsado==cazas[1].totalCupo?Colors.orangeAccent:Colors.green).withOpacity(0.8),
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Center(
                                child: Text(cazas[1].totalUsado.toString()+"/"+cazas[1].totalCupo.toString(),
                                    style:TextStyle(color: color4,
                                    fontSize: size.width*0.05,fontWeight: FontWeight.w500)),
                              ),
                            )
                          ],
                        ),
                      )
                  ):Container(),
                ],
              ),
            ),
        ],
      ),
    ),
    );
  }
}
