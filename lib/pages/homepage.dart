import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/loginBloc.dart';
import 'package:sidebar_animation/events/NavigationEvent.dart';
import 'package:sidebar_animation/events/loginEvent.dart';
import 'package:sidebar_animation/model/Caza.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';
import '../core/color.dart';

class HomePage extends StatelessWidget with NavigationStates {
  Size size;
  HomePage(this.cazasLocal);
  List<Caza> cazasLocal=List();
  @override
  Widget build(BuildContext context) {
    size=MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: color1,
      body: Container(
              height: size.height,
              width: size.width,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: size.height/3,
                    width: size.height,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/imgcascada2.jpg"),
                          fit: BoxFit.fill
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: (size.height*0.4)-70),
                    height: size.height,
                    width: size.width,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft:Radius.circular(40),
                        topRight:Radius.circular(40),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                            BlocProvider.of<NavigationBloc>(context).add(ReporteClickedEvent());
                          },
                          child: Container(
                            height: size.height*0.4/2,
                            width: size.height*0.4/2,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(16),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0,3),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Icon(
                                  Icons.equalizer,
                                  color: color1,
                                  size: size.height*0.4/2-30,
                                ),
                                Text("Realizar Reporte", textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: size.height*0.4/2*0.1),),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: size.height*0.05,
                        ),
                        GestureDetector(
                          onTap: (){
                            BlocProvider.of<NavigationBloc>(context).add(HistorialClickedEvent());
                          },
                          child: Container(
                            height: size.height*0.4/2,
                            width: size.height*0.4/2,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(16),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0,3),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Icon(
                                  Icons.show_chart,
                                  color: color1,
                                  size: size.height*0.4/2-35,
                                ),
                                Text("Historial de Reportes", textAlign: TextAlign.center,style: TextStyle(color: color1,fontSize: size.height*0.4/2*0.09,),),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: size.height*0.05,
                        ),
                        GestureDetector(
                          onTap: (){
                            BlocProvider.of<NavigationBloc>(context).add(ReportesLocalClickedEvent(cazasLocal,context));
                          },
                          child: Container(
                            height: size.height*0.4/5,
                            width: size.height*0.4/2,
                            decoration: BoxDecoration(
                              color: cazasLocal.length>0?Colors.redAccent:Colors.green.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(16),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0,3),
                                ),
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [

                                Text(cazasLocal.length.toString(), textAlign: TextAlign.center,style: TextStyle(color: cazasLocal.length>0?color4:color1,fontSize: size.height*0.4*0.08,),),
                                Text("Reportes sin Enviar", textAlign: TextAlign.center,style: TextStyle(color: cazasLocal.length>0?color4:color1,fontSize: size.height*0.4/2*0.1,),),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 30,
                    top: (size.height*0.4)/2-20,
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Inicio',
                          style: TextStyle(color:Colors.white, fontSize:40 ),
                          ),
                      ],
                    ),
                  ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      child:GestureDetector(
                        onTap: (){
                          BlocProvider.of<LoginBloc>(context).add(CerrarLogin());
                        },
                        child: Container(
                          height: size.width*0.18,
                          width: size.width*0.18,
                          decoration: BoxDecoration(
                            color: color1,
                            borderRadius: BorderRadius.only(topLeft:Radius.circular(20),topRight:Radius.zero,bottomLeft:Radius.zero,bottomRight: Radius.zero),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.9),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0,3),
                              ),
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                                  Icon(
                                    Icons.input,
                                    color: Colors.white,
                                    size: size.width*0.06,
                                  ),
                              AutoSizeText(
                                'Salir',
                                style: TextStyle(color:Colors.white, fontSize:size.width*0.08*0.5),
                              ),
                            ],
                          ),
                        ),
                      ),
                  ),
                ],
              ),
            ),
    );
  }
}
