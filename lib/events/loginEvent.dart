
import 'package:equatable/equatable.dart';
import '../model/UserLogin.dart';

abstract class LoginEvent extends Equatable {}

class LoginClick extends LoginEvent {
  userLogin login;
  LoginClick(this.login);
  @override
  List<Object> get props => [login];
}
class VerificarLogin extends LoginEvent {
  VerificarLogin();
  @override
  List<Object> get props => [];
}
class CerrarLogin extends LoginEvent {
  CerrarLogin();
  @override
  List<Object> get props => [];
}