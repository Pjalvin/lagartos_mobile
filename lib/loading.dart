//Esta es mi pantalla de carga donde verifica el login

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sidebar_animation/events/loginEvent.dart';
import './bloc.navigation_bloc/loginBloc.dart';
import './core/color.dart';


class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //AuthUser().cerrarSesion(context);
    BlocProvider.of<LoginBloc>(context).add(VerificarLogin());
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return
      Scaffold(
        backgroundColor: color1,
        body: Container(
          height: size.height,
          width: size.width,
          child: Column(
            children: <Widget>[
              Expanded(
                  child: Center(
                    child: Image(
                      image: AssetImage("assets/images/logo_mmaya_white.png"),
                      width: size.width * 0.4,),
                  )

              ),
              Container(
                  height: size.height * 0.5,
                  width: size.width,
                  child: Center(
                      child: Container(
                        height: 30,
                        width: 30,
                        child: CircularProgressIndicator(
                          backgroundColor: color3,

                          valueColor: new AlwaysStoppedAnimation<Color>(color2),
                        ),
                      )
                  )
              )
            ],
          ),
        )
      );
  }
}