import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/loginBloc.dart';
import 'package:sidebar_animation/events/NavigationEvent.dart';
import 'package:sidebar_animation/events/loginEvent.dart';
import 'package:sidebar_animation/repository/GeoRepository.dart';
import 'package:sidebar_animation/repository/ReportsRepository.dart';
import 'package:sidebar_animation/states/exitLogin.dart';

import '../bloc.navigation_bloc/navigation_bloc.dart';
import 'sidebar.dart';

// class SideBarLayout extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: BlocProvider<NavigationBloc>(
//         create: (context) => NavigationBloc(ReportsRepository(),GeoRepository()),
//         child: Stack(
//           children: <Widget>[
//             BlocBuilder<NavigationBloc, NavigationStates>(
//               builder: (context, navigationState) {
//                 return navigationState as Widget;
//               },
//             ),
//             SideBar(),
//           ],
//         ),
//       ),
//     );
//   }
// }
class SideBarLayout extends StatefulWidget {
  @override
  _SideBarLayoutState createState() => _SideBarLayoutState();
}

class _SideBarLayoutState extends State<SideBarLayout> {

  bool retroceder=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider<NavigationBloc>(
          create: (context) => NavigationBloc(ReportsRepository(),GeoRepository())..add(InitEvent()),
          child:Stack(
              children: <Widget>[
                BlocBuilder<NavigationBloc, NavigationStates>(
                  builder: (context, navigationState) {
                        if(navigationState is ExitLogin){
                          BlocProvider.of<LoginBloc>(context).add(CerrarLogin());
                          return Container();
                        }
                        else{

                          return WillPopScope(
                              onWillPop: ()async {
                                if(navigationState.toString()!="HomePage"){

                                  BlocProvider.of<NavigationBloc>(context).add(HomePageClickedEvent());
                                  return null;
                                }
                                else{
                                  return true;
                                }
                              },
                              child:navigationState as Widget
                          );
                        }

                  },
                ),
                SideBar(),
              ],
            ),

        ),
      );
  }
}
