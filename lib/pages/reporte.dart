import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:preload_page_view/preload_page_view.dart';
import 'package:sidebar_animation/model/Caza.dart';
import 'package:sidebar_animation/pages/ubicacion.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';
import '../core/color.dart';
class Reporte extends StatefulWidget with NavigationStates{
  @override
  _ReporteState createState() => _ReporteState();
  Reporte(this.posicion);
  Position posicion;
}

class _ReporteState extends State<Reporte> {
  bool next=false,previous=false;
  Caza caza=Caza();
  @override
  void initState() {
    super.initState();
    Ubicacion(widget.posicion, caza);
  }

  var controllerpage=PreloadPageController();
  Size size;
  @override
  Widget build(BuildContext context) {
    size=MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: color1,
      body: SingleChildScrollView(
        child: Container(
          width: size.width,
          child: SizedBox(
                  height: size.height,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: size.height*0.4,
                        width: size.height,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/imgreporte2.jpg"),
                              fit: BoxFit.fill
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: (size.height*0.4)-70),
                        height: size.height,
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0),
                          borderRadius: BorderRadius.only(
                            topLeft:Radius.circular(40),
                            topRight:Radius.circular(40),
                          ),
                        ),
                      ),
                      Positioned(
                        right: 30,
                        top: (size.height*0.4)/2-20,
                        child: Column(
                          children: <Widget>[
                            Text(
                              'Reporte',
                              style: TextStyle(color:Colors.white, fontSize:40 ),
                              ),
                          ],
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        child: Container(
                          height: size.height*0.7,
                          width: size.width,
                          child: Ubicacion(widget.posicion, caza),
                        ),
                      ),
                    ],
                  ),
          ),
    ),
      ),
    );
  }
}


