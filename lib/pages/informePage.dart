import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sidebar_animation/core/color.dart';
import 'package:custom_dropdown/custom_dropdown.dart';
import 'package:sidebar_animation/libs/MyBehavior.dart';
import 'package:sidebar_animation/model/DropDownItem.dart';
import 'package:sidebar_animation/model/EstadoTco.dart';
import 'package:sidebar_animation/repository/EstadoTco.dart';
class InformePage extends StatefulWidget {
  @override
  _InformePageState createState() => _InformePageState();
}

class _InformePageState extends State<InformePage> {
  // int _value = 1;
  bool load=false;
  EstadoTco estadoTco;
  List<DropDownItem> departamentos=List();
  DropDownItem _selDepto=DropDownItem();
  DropDownItem _selMunic=DropDownItem();
  DropDownItem _selProvi=DropDownItem();
  DropDownItem _selTco=DropDownItem();
  List<DropDownItem> provincias=List();
  List<DropDownItem> municipios=List();
  List<DropDownItem> tcos=List();
  Size size;
  void initState(){
    cargarDepartamentos();
  }
  cargarEstado()async {
    setState(() {
      load=true;
      estadoTco=null;
    });
    EstadoTcoRepository estadoTcoRepository=EstadoTcoRepository();
    EstadoTco estadoAux=await estadoTcoRepository.obtenerEstado(tcos[_selTco.id].id);
    setState(() {
      estadoTco=estadoAux;
      load=false;
    });
  }
  cargarDepartamentos()async{
    try{
      setState(() {
        load=true;
        provincias=List();
        municipios=List();
        tcos=List();
        estadoTco=null;
      });
      EstadoTcoRepository estadoTcoRepository=EstadoTcoRepository();
      List deptos=await estadoTcoRepository.obtenerDepartamentos();
      List<DropDownItem> deptosAux=List();
      deptos.forEach((element) {
        DropDownItem dropDownItem=DropDownItem();
        dropDownItem.id=element.itemId;
        dropDownItem.valor=element.nombreDepartamento;
        deptosAux.add(dropDownItem);
      });
      await Future.delayed(Duration(seconds: 1));
      setState(() {
        departamentos=deptosAux;
      });
      setState(() {
        load=false;
      });
    }
    catch(e){
    }
  }
  cargarProvincias()async{
    setState(() {
      load=true;
      municipios=List();
      tcos=List();
      _selMunic=DropDownItem();
      _selProvi=DropDownItem();
      _selTco=DropDownItem();
      estadoTco=null;
    });
    EstadoTcoRepository estadoTcoRepository=EstadoTcoRepository();
    List provs=await estadoTcoRepository.obtenerProvincias(departamentos[_selDepto.id].id);
    List<DropDownItem> provsAux=List();
    provs.forEach((element) {
      DropDownItem dropDownItem=DropDownItem();
      dropDownItem.id=element.itemId;
      dropDownItem.valor=element.nombreProvincia;
      provsAux.add(dropDownItem);
    });
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      provincias=provsAux;
    });
    setState(() {
      load=false;
    });

  }
  cargarMunicipio()async{
    setState(() {
      load=true;
      tcos=List();
      _selMunic=DropDownItem();
      _selTco=DropDownItem();
      estadoTco=null;
    });
    EstadoTcoRepository estadoTcoRepository=EstadoTcoRepository();
    List munis=await estadoTcoRepository.obtenerMunicipios(provincias[_selProvi.id].id);
    List<DropDownItem> munisAux=List();
    munis.forEach((element) {
      DropDownItem dropDownItem=DropDownItem();
      dropDownItem.id=element.itemId;
      dropDownItem.valor=element.nombreMunicipio;
      munisAux.add(dropDownItem);
    });
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      municipios=munisAux;
    });
    setState(() {
      load=false;
    });
  }
  cargarTco()async{
    setState(() {
      load=true;
      estadoTco=null;
    });
    EstadoTcoRepository estadoTcoRepository=EstadoTcoRepository();
    List tc=await estadoTcoRepository.obtenerTcos(municipios[_selMunic.id].id);
    List<DropDownItem> tcosAux=List();
    tc.forEach((element) {
      DropDownItem dropDownItem=DropDownItem();
      dropDownItem.id=element.itemId;
      dropDownItem.valor=element.tco;
      tcosAux.add(dropDownItem);
    });
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      tcos=tcosAux;
    });
    setState(() {
      load=false;
    });
  }

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems){
    List<DropdownMenuItem<ListItem>>
    items=List();
    for(ListItem listItem in listItems){
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }
  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems2(List listItems2){
    List<DropdownMenuItem<ListItem>>
    items2=List();
    for(ListItem listItem in listItems2){
      items2.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items2;
  }
  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems3(List listItems3){
    List<DropdownMenuItem<ListItem>>
    items3=List();
    for(ListItem listItem in listItems3){
      items3.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items3;
  }
  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems4(List listItems4){
    List<DropdownMenuItem<ListItem>>
    items4=List();
    for(ListItem listItem in listItems4){
      items4.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items4;
  }
  // int _value = 1;
  @override
  Widget build(BuildContext context) {
    size=MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFF225957),
      body: Column(
        children: [
          Container(
            height: size.height,
            width: size.width,
            child: Center(
              child: Stack(
                children: [
                  Container(
                    height: size.height*0.2,
                    width: size.height,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/imghojas.jpg"),
                          fit: BoxFit.fill
                      ),
                    ),
                  ),

                  // SizedBox(height: size.height*0.5,),
                  Container(
                    height: size.height*0.8,
                    width: size.width,
                    margin: EdgeInsets.only(top: size.height*0.2),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.4),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0,3),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 30,
                    top: (size.height*0.1),
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Información',
                          style: TextStyle(color:Colors.white, fontSize:size.height*0.05 ),
                        ),
                      ],
                    ),
                  ),

                  // SizedBox(height: size.height*0.01,),
                  Container(
                    padding: EdgeInsets.only(top: size.height*0.2),
                    child: ScrollConfiguration(
                      behavior: MyBehavior(),
                      child: ListView(
                        padding: EdgeInsets.only(top:  size.height*0.05,bottom: size.height*0.05),
                        children: [
                          dropdownLabel(_selDepto, departamentos, "Departamento"),
                          dropdownLabel(_selProvi, provincias, "Provincia"),
                          dropdownLabel(_selMunic, municipios, "Municipio"),
                          dropdownLabel(_selTco, tcos, "TCO"),
                          SizedBox(height: size.height*0.02,),
                          Container(
                            height: 130,
                            margin: EdgeInsets.symmetric(horizontal: size.width*0.1),
                            width: size.width*0.8,
                            // color: Colors.blue,
                            decoration: BoxDecoration(
                              // color: color2,
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0,3),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Container(
                                  // color: color4,
                                  color: Color(0xFF225957),
                                  height:  30,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      // Text("Reporte",style: TextStyle(color: color1,fontSize: 20,),),
                                      Container(

                                        child:Text("Estado",style: TextStyle(color: Colors.white,fontSize: 20,),),
                                      ),
                                    ],
                                  ),
                                ),

                                Expanded(
                                  child: Container(
                                    child:estadoTco==null?Container():Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        datos(estadoTco.tenencia==null?"0":estadoTco.tenencia.toString(),"Cueros en tenencia"),
                                        datos(estadoTco.entregado==null?"0":estadoTco.entregado.toString(),"Cueros entregados"),
                                        datos(estadoTco.vendido==null?"0.0 Bs.":estadoTco.vendido.toString()+" Bs","Venta de Cuero"),
                                      ],
                                    )
                                  ),

                                )
                              ],
                            ),
                          ),
                          SizedBox(height: size.height*0.02,),
                          Container(

                            margin: EdgeInsets.symmetric(horizontal: size.width*0.3),
                            child: RaisedButton(
                                color:color1,
                                child: Text("Volver",style: TextStyle(color:Colors.white,fontSize: size.width*0.04),),
                                shape: RoundedRectangleBorder(

                                  borderRadius: new BorderRadius.circular(18.0),

                                ),
                                onPressed: (){
                                  Navigator.pop(context);
                                }

                            ),
                          )
                        ],
                      ),
                    ),
                  ),

                  // Positioned(
                  //   bottom: 0,
                  //   right: 0,
                  //   child:GestureDetector(
                  //     onTap: (){
                  //       // BlocProvider.of<LoginBloc>(context).add(CerrarLogin());
                  //     },
                  //     child: Container(
                  //       height: size.width*0.18,
                  //       width: size.width*0.18,
                  //       decoration: BoxDecoration(
                  //         // color: color1,
                  //         color: Color(0xFF225957),
                  //         borderRadius: BorderRadius.only(topLeft:Radius.circular(50),topRight:Radius.zero,bottomLeft:Radius.zero,bottomRight: Radius.zero),
                  //         boxShadow: [
                  //           BoxShadow(
                  //             color: Colors.grey.withOpacity(0.9),
                  //             spreadRadius: 5,
                  //             blurRadius: 7,
                  //             offset: Offset(0,3),
                  //           ),
                  //         ],
                  //
                  //       ),
                  //       child: Column(
                  //         mainAxisAlignment: MainAxisAlignment.center,
                  //         children: [
                  //           MaterialButton(
                  //             padding: EdgeInsets.all(0),
                  //             child:
                  //             Icon(
                  //               Icons.input,
                  //               color: Colors.white,
                  //               size: size.width*0.08,
                  //             ),
                  //           ),
                  //           Text(
                  //             'Salir',
                  //             style: TextStyle(color:Colors.white, fontSize:size.width*0.08*0.4),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),

                  load?loading():Container(),
                ],
              ),
            ),
          ),

        ],
      ),
      // ),
    );
  }
  Widget datos(dato,label){
    return
      Container(
        width: (size.width*0.8)/3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: AutoSizeText(dato,textAlign:TextAlign.center,style:TextStyle(color: color1,fontSize: size.height*0.038),maxLines: 1,)
            ),
            Container(
              child: Text(label,textAlign:TextAlign.center,style:TextStyle(color: color1,fontSize: size.height*0.02)),
            )
          ],
        ),
      );
  }
  Widget loading(){
    return Container(
      width: size.width,
      height: size.height,
      color: color5.withOpacity(0.3),
      child: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.transparent,
          valueColor: new AlwaysStoppedAnimation<Color>(color2),
        ),
      ),
    );
  }
  Widget dropdownLabel( DropDownItem value,List<DropDownItem>items,label){
    return Container(
      height: 70,
      width: size.width,
      padding: EdgeInsets.only(left: size.height*0.06,right: size.height*0.06),
      // color: Colors.yellow,
      child:Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            children: [
              Container(
                // margin: EdgeInsets.only(top: (size.height*0.4)),
              width: size.width-size.height*0.06*2,
                decoration: BoxDecoration(
                  color: Color(0xFFEEEBD3),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      spreadRadius: 2,
                      blurRadius: 15,
                      offset: Offset(0,3),
                    ),
                  ],
                ),
                child:items.length>0?CustomDropdown(

                  enabledIconColor: color1,
                          enableTextColor: color1,
                          openColor: color2,
                          elementTextColor: color1,



                          items: items.map((item){
                            return CustomDropdownItem(text: item.valor);
                          }).toList(),
                          hint: "Selecciona "+label,
                          disabledColor: color1.withOpacity(0.5),
                          disabledTextColor: color5.withOpacity(0.3),
                          disabledIconColor: color5.withOpacity(0.3),
                          valueIndex: value==null?null:value.id,
                          onChanged: (val){
                            if(value.id!=val){
                              setState(() {
                                value.id=val;
                              });
                              switch(label){
                                case "Departamento":

                                  cargarProvincias();
                                  break;
                                case "Provincia":
                                  cargarMunicipio();
                                  break;
                                case "Municipio":
                                  cargarTco();
                                  break;
                                case "TCO":
                                  cargarEstado();
                                  break;
                              }
                            }
                          },
                        ):
                  Container()
              ),
            ],
          ),
        ],
      )
    );
  }


}

class ListItem {
  int value;
  String name;

  ListItem(this.value, this.name);
}
