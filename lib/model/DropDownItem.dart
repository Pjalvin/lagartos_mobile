class DropDownItem{
  int _id;
  String _valor;

  DropDownItem();

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get valor => _valor;

  set valor(String value) {
    _valor = value;
  }
}