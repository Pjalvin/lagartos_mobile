class Departamento{
  int _itemId;
  String _nombreDepartamento;
  Departamento();

  String get nombreDepartamento => _nombreDepartamento;

  set nombreDepartamento(String value) {
    _nombreDepartamento = value;
  }

  int get itemId => _itemId;

  set itemId(int value) {
    _itemId = value;
  }

  Map toJson() => {
    'itemId': itemId,
    'nombre_departamento': nombreDepartamento,
  };
}