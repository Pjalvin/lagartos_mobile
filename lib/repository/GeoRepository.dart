import 'package:geolocator/geolocator.dart';
class GeoRepository{
  Future<Position> obtenerUbicacion()async {
    try{
      Position position = await getLastKnownPosition();
      return position;

    }
    catch(e){
      return null;
    }

  }



}