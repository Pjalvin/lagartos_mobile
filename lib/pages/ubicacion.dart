import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/noConectionBloc.dart';
import 'package:sidebar_animation/core/color.dart';
import 'package:sidebar_animation/events/NavigationEvent.dart';
import 'package:sidebar_animation/events/noConectionEvent.dart';
import 'package:sidebar_animation/model/Caza.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';
class Ubicacion extends StatefulWidget with NavigationStates{

  var state;
  @override
  _UbicacionState createState() {
    state=_UbicacionState(posicion,caza);
    return state;}
  Position posicion;
  Caza caza;

  Ubicacion(this.posicion, this.caza);
}
class _UbicacionState extends State<Ubicacion> {
  Caza caza;
  TextEditingController _cantController= TextEditingController();
  _UbicacionState(this.posicion,this.caza){
    if(this.posicion!=null){
    _kGooglePlex=CameraPosition(
      target: LatLng( posicion.latitude,posicion.longitude),
      zoom: 14.4746,
    );
    var  mark= Marker(
      markerId: MarkerId("curr_loc"),
      position: LatLng(posicion.latitude,posicion.longitude),
      infoWindow: InfoWindow(title: 'Tu ubicacion'),);

    markers.add(mark);}
    else{
        _kGooglePlex=CameraPosition(
          target: LatLng( 1,1),
          zoom: 14.4746,
        );
        var  mark= Marker(
          markerId: MarkerId("curr_loc"),
          position: LatLng(1,1),
          infoWindow: InfoWindow(title: 'Tu ubicacion'),);

        markers.add(mark);
    }
  }
  final Position posicion;
  Set<Marker> markers=Set();
  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex;
 bool mapaActivo=true;
  bool mapaCreado=true;
  Size size;
  @override
  Widget build(BuildContext context) {
    size=MediaQuery.of(context).size;
    return Container(
      height: size.height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft:Radius.circular(40),
          topRight:Radius.circular(40),
        ),
      ),
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: size.height*0.05,
          ),
          mapaActivo?Container(
            height: size.height*0.3,
            width: size.height*0.3,
            decoration: BoxDecoration(
              color: color5,
              borderRadius: BorderRadius.circular(size.height*0.01)
            ),
            child: Stack(
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(size.height*0.01),
                    child: GoogleMap(
                      mapType: MapType.hybrid,
                      initialCameraPosition: _kGooglePlex,
                      markers: markers,

                      onMapCreated: (GoogleMapController controller) {
                        !_controller.isCompleted?_controller.complete(controller):null;
                        Timer(Duration(milliseconds: 700), () {
                         setState(() {
                           mapaCreado = true;
                         });
                        });
                      },
                    )
                ),
                !mapaCreado?Container(
                  color: color2,
                  height: size.height*0.4,
                  width: size.height*0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(
                        backgroundColor: color2,

                        valueColor: new AlwaysStoppedAnimation<Color>(color1),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text("Cargando Mapa",style: TextStyle(color: color1),)
                    ],
                  ),
                ):Container()
              ],
            )
          ):Container(
            color: color5,
            height: size.height*0.3,
            width: size.height*0.3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  backgroundColor: color5,

                  valueColor: new AlwaysStoppedAnimation<Color>(color2),
                ),
                SizedBox(
                  height: 10,
                ),
                Text("Cargando Mapa",style: TextStyle(color: color2),)
              ],
            ),
          ),
          SizedBox(
            height: size.height*0.07,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text("Cantidad Lagartos:",style: TextStyle(color: color1,fontSize: 19),),
              SizedBox(
                width: size.width/15,
              ),
              Container(
                height: size.height/16,
                width: size.width/5,
                decoration: BoxDecoration(
                  color: color4,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0,3),
                    ),
                  ],
                ),
                // child: TextFieldContainer(
                child: Center(
                  child: TextField(
                    cursorColor: color1,
                    textAlign: TextAlign.center,
                    // maxLength: 5,
                    style: TextStyle(color: color1,fontSize: 18,),
                    decoration: null,
                    maxLength: 5,
                    maxLines: 1,
                    keyboardType: TextInputType.number,
                    controller: _cantController,
                  ),
                ),
                // ),
              ),
              SizedBox(
                width: size.width*0.09,
              ),
            ],
          ),
          SizedBox(
            height: size.height*0.07,
          ),
          RaisedButton(
              padding: EdgeInsets.only(left: 15,right: 15,top: 10,bottom: 10),
              color: color1,
              child: Text(
                'Enviar',
                style: TextStyle(color:color2, fontSize:size.width*0.08*0.7 ),
              ),
              onPressed: (){
                  caza.cantidad=int.parse(_cantController.text);
                  if(posicion==null){
                    BlocProvider.of<NoConectionBloc>(context).add(NoConectionReporteClick(caza));
                  }
                  else{

                    caza.coorY=posicion.longitude;
                    caza.coorX=posicion.latitude;
                    BlocProvider.of<NavigationBloc>(context).add(reporteClickedEvent(caza));
                  }

              }
          ),
        ],
      ),
    );
  }
  recargar(){
    setState(() {
      mapaActivo=!mapaActivo;
      mapaCreado=false;
    });
  }
  Completer<GoogleMapController> get controller => _controller;

  set controller(Completer<GoogleMapController> value) {
    _controller = value;
  }

  TextEditingController get cantController => _cantController;
}



