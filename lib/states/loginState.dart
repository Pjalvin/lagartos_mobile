
import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}
class LoginInit extends LoginState{

  const LoginInit();

  @override
  List<Object> get props => [];
}
class LoginLoading extends LoginState{

  const LoginLoading();

  @override
  List<Object> get props => [];
}
class LoginOk extends LoginState{

  const LoginOk();

  @override
  List<Object> get props => [];
}
class LoginFailed extends LoginState{

  const LoginFailed();

  @override
  List<Object> get props => [];
}
class NoConection extends LoginState{

  const NoConection();

  @override
  List<Object> get props => [];
}
class NoGPS extends LoginState{

  @override
  List<Object> get props => [];
}
