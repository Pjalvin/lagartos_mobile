import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidebar_animation/bloc.navigation_bloc/loginBloc.dart';
import 'package:sidebar_animation/events/NavigationEvent.dart';
import 'package:sidebar_animation/events/loginEvent.dart';

import '../bloc.navigation_bloc/navigation_bloc.dart';
import '../sidebar/menu_item.dart';
import '../core/color.dart';

class SideBar extends StatefulWidget {
  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> with SingleTickerProviderStateMixin<SideBar> {
  AnimationController _animationController;
  StreamController<bool> isSidebarOpenedStreamController;
  Stream<bool> isSidebarOpenedStream;
  StreamSink<bool> isSidebarOpenedSink;
  final _animationDuration = const Duration(milliseconds: 500);
  String nombre="";
  String email="";
  void obtenerPerfil()async{
    SharedPreferences sha=await SharedPreferences.getInstance();
    setState(() {
      nombre= sha.getString("nombre");
      email=sha.getString("email");
    });
  }
  @override
  void initState() {
    super.initState();
    _animationController = AnimationController( duration: _animationDuration,vsync: this);
    isSidebarOpenedStreamController = PublishSubject<bool>();
    isSidebarOpenedStream = isSidebarOpenedStreamController.stream;
    isSidebarOpenedSink = isSidebarOpenedStreamController.sink;
    obtenerPerfil();
  }

  @override
  void dispose() {
    _animationController.dispose();
    isSidebarOpenedStreamController.close();
    isSidebarOpenedSink.close();
    super.dispose();
  }

  void onIconPressed() {
    final animationStatus = _animationController.status;
    final isAnimationCompleted = animationStatus == AnimationStatus.completed;

    if (isAnimationCompleted) {
      isSidebarOpenedSink.add(false);
      _animationController.reverse();
    } else {
      isSidebarOpenedSink.add(true);
      _animationController.forward();
    }
  }
  Size size;
  double screenWidth;
  @override
  Widget build(BuildContext context) {
    screenWidth= MediaQuery.of(context).size.width;
    size=MediaQuery.of(context).size;
    return StreamBuilder<bool>(
      initialData: false,
      stream: isSidebarOpenedStream,
      builder: (context, isSideBarOpenedAsync) {
        size=MediaQuery.of(context).size;
        return Stack(
          children: <Widget>[
            Align(
              child: Container(
                width: isSideBarOpenedAsync.data ?size.width:0,
                color: isSideBarOpenedAsync.data ?color5.withOpacity(0.5):Colors.transparent,
                child:  GestureDetector(
                  onTap: (){
                    onIconPressed();

                  },

                ),
              ),
            ),

            AnimatedPositioned(
              duration: _animationDuration,
              top: 0,
              bottom: 0,
              left: isSideBarOpenedAsync.data ? 0 : -screenWidth+90,
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      width: size.width-90,

                      child: Stack(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(0),
                            width: screenWidth - 90,
                            height: size.height,
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.symmetric(vertical: size.height*0.08),
                                  child: ListTile(


                                    title: AutoSizeText(
                                      nombre,
                                      style: TextStyle(color: Color(0xFF225957), fontSize: 30, fontWeight: FontWeight.w800),
                                      maxLines: 2,
                                    ),

                                    subtitle: AutoSizeText(
                                      email,
                                      style: TextStyle(
                                        height: 2,
                                        color: color1.withOpacity(0.8),
                                        fontSize: 18,
                                      ),
                                      maxLines: 1,
                                    ),
                                    leading: CircleAvatar(
                                      backgroundColor: color1,
                                      child: Icon(
                                        Icons.perm_identity,
                                        color: color2,
                                      ),
                                      radius: size.width*0.1,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Color(0xFF225957),
                                      borderRadius: BorderRadius.only(
                                        topLeft:Radius.circular(40),
                                        topRight:Radius.circular(40),
                                      ),
                                    ),
                                    child: ScrollConfiguration(
                                      behavior: MyBehavior(),
                                        child: ListView(
                                          children: <Widget>[
                                            SizedBox(
                                              height: 30,
                                            ),
                                            MenuItem(
                                              icon: Icons.home,
                                              title: "Inicio",
                                              onTap: () {
                                                onIconPressed();
                                                BlocProvider.of<NavigationBloc>(context).add(HomePageClickedEvent());
                                              },
                                            ),
                                            MenuItem(
                                              icon: Icons.person,
                                              title: "Reporte",
                                              fontSize: 18,
                                              onTap: () {
                                                onIconPressed();
                                                BlocProvider.of<NavigationBloc>(context).add(ReporteClickedEvent());
                                              },
                                            ),
                                            MenuItem(
                                              icon: Icons.shopping_basket,
                                              title: "Historial",
                                              onTap: () {
                                                onIconPressed();
                                                BlocProvider.of<NavigationBloc>(context).add(HistorialClickedEvent());
                                              },
                                            ),
                                            Divider(
                                              height: 50,
                                              thickness: 0.5,
                                              //color: Colors.white.withOpacity(0.3),
                                              color: Colors.white,
                                              indent: 32,
                                              endIndent: 32,
                                            ),
                                            /*MenuItem(
                                              icon: Icons.settings,
                                              title: "Settings",
                                            ),*/
                                            MenuItem(
                                              icon: Icons.exit_to_app,
                                              title: "Cerrar Sesión",
                                              onTap: (){
                                                onIconPressed();
                                                BlocProvider.of<LoginBloc>(context).add(CerrarLogin());
                                              },
                                            ),
                                          ],
                                        ),
                                    )
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment(0, -0.7),
                      child: GestureDetector(
                        onTap: () {
                          onIconPressed();
                        },
                        child: ClipPath(
                          clipper: CustomMenuClipper(),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color:color2),
                              color: Colors.white,
                            ),
                            width: 35,
                            height: 110,
                            alignment: Alignment.centerLeft,
                            child: AnimatedIcon(
                              progress: _animationController.view,
                              icon: AnimatedIcons.menu_close,
                              color: Color(0xFF225957),
                              size: 25,//tamaño icono del menu
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}

class CustomMenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color = Colors.white;

    final width = size.width;
    final height = size.height;

    Path path = Path();
    path.moveTo(0, 0);
    path.quadraticBezierTo(0, 8, 10, 16);
    path.quadraticBezierTo(width - 1, height / 2 - 20, width, height / 2);
    path.quadraticBezierTo(width + 1, height / 2 + 20, 10, height - 16);
    path.quadraticBezierTo(0, height - 8, 0, height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }

}
class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
