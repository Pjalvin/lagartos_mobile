class Municipio{
  int _itemId;
  String _nombreMunicipio;
  int _provinciaItemId;
  Municipio();

  String get nombreMunicipio => _nombreMunicipio;

  set nombreMunicipio(String value) {
    _nombreMunicipio = value;
  }

  int get itemId => _itemId;

  set itemId(int value) {
    _itemId = value;
  }

  int get provinciaItemId => _provinciaItemId;

  set provinciaItemId(int value) {
    _provinciaItemId = value;
  }

  Map toJson() => {
    'itemId': itemId,
    'nombre_municipio': nombreMunicipio,
    'provincia_itemId': provinciaItemId
  };
}