
import 'package:equatable/equatable.dart';

abstract class NoConectionState extends Equatable {
  const NoConectionState();
}
class NoConectionReporte extends NoConectionState{

  const NoConectionReporte();

  @override
  List<Object> get props => [];
}
class NoConectionReporteOnline extends NoConectionState{

  const NoConectionReporteOnline();

  @override
  List<Object> get props => [];
}
class NoConectionLoading extends NoConectionState{

  const NoConectionLoading();

  @override
  List<Object> get props => [];
}
