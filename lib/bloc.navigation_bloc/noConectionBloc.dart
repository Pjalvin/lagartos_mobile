import 'package:bloc/bloc.dart';
import 'package:sidebar_animation/events/noConectionEvent.dart';
import 'package:sidebar_animation/repository/ReportsRepository.dart';
import 'package:sidebar_animation/states/noConectionState.dart';



class NoConectionBloc extends Bloc<NoConectionEvent, NoConectionState> {
  ReportsRepository reportRepository;
  NoConectionBloc(this.reportRepository);
  @override
  NoConectionState get initialState => NoConectionReporte();

  @override
  Stream<NoConectionState> mapEventToState(NoConectionEvent event) async* {
    if(event is NoConectionReporteClick){
      yield NoConectionLoading();
      var reporte= await reportRepository.subirReporteCaza(event.caza);
      if(reporte==2){
        yield NoConectionReporte();
      }
      else if(reporte==1){
        yield NoConectionReporteOnline();

      }
      else{
        yield NoConectionReporte();
      }
    }
  }

}
