import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidebar_animation/model/UserLogin.dart';
import 'package:http/http.dart' as http;
import '../core/apiUrl.dart' as api;
import '../libs/verifyConnection.dart'as verify;
import '../libs/Toast.dart'as Toast;
class LoginRepository{
  Future<bool>iniciarSesion(userLogin login)async {
    bool ver=await verify.verify();
    if(ver){
      try{
        final response=await  http.post(api.url+"/login",
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'authorization': env["API_TOKEN"]
          },
          body: jsonEncode(login.toJson()));
        if(response.statusCode==200){
          SharedPreferences sha=await SharedPreferences.getInstance();
          await sha.setString("token", response.body);
          await obtenerPerfil();
          return true;
        }
        else{
          if(response.statusCode==425){

            Toast.toastError("No está habilitado para realizar reportes en esta fecha.");
            return false;
          }
          else{
            return false;
          }
        }
      }
      catch(e){
        return false;
      }
    }
    else{
      Toast.toastError("Sin conexión");
      SharedPreferences sha=await SharedPreferences.getInstance();
      await sha.setInt("itemId", -1);
      await sha.setString(
          "nombre", "Sin Conexión");
      await sha.setString("email", "-");
      return true;
    }

  }
  Future<bool>cerrarSesion()async {
    bool ver=await verify.verify();
    if(ver){
      try{
        SharedPreferences sha=await SharedPreferences.getInstance();
        bool b=await sha.remove("token");
        if(b){
          return true;
        }
        else{
          return false;
        }

      }
      catch(e) {
        return false;
      }
    }
    else{
    Toast.toastError("Sin conexión");
    return true;
    }

  }
  Future<int>obtenerPerfil()async {

    bool ver=await verify.verify();
    if(ver==null){
      return 3;
    }
    else if(ver) {
      try {
        SharedPreferences sha = await SharedPreferences.getInstance();
        String token = sha.getString("token");
        if(token!=null){
          final response = await http.get(api.url + "/profile/",
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
                'authorizationApp': token,
                'authorization': env["API_TOKEN"]
              }
          ).timeout(Duration(seconds: 20));
          if (response.statusCode == 200) {
            await guardarDatosPerfil(response.body);
            return 1;
          }
          else {
            if(response.statusCode==425){

              Toast.toastError("No está habilitado para realizar reportes en esta fecha.");
              return 0;
            }
            else{
              return 0;
            }
          }
        }
        else{
          return 0;
        }
      }
      catch (e) {
        return 0;
      }
    }
    else{
      Toast.toastError("Sin conexión");
      SharedPreferences sha=await SharedPreferences.getInstance();
      await sha.setInt("itemId", -1);
      await sha.setString(
          "nombre", "Sin Conexión");
      await sha.setString("email", "-");
      return 2;
    }

  }
  guardarDatosPerfil(body)async{

    bool ver=await verify.verify();
    if(ver) {
      var datos = await jsonDecode(body);
      SharedPreferences sha = await SharedPreferences.getInstance();
      await sha.setInt("itemId", datos[0]["itemId"]);
      await sha.setString(
          "nombre", datos[0]["nombre"] + " " + datos[0]["apellido"]);
      await sha.setString("email", datos[0]["email"]);
    }
    else{
      Toast.toastError("Sin conexión");
    }
  }




}