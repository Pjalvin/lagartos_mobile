
import 'package:equatable/equatable.dart';
import 'package:sidebar_animation/model/Caza.dart';

abstract class NoConectionEvent extends Equatable {}

class NoConectionReporteClick extends NoConectionEvent {
  Caza caza;
  NoConectionReporteClick(this.caza);
  @override
  List<Object> get props => [caza];
}
class NoConectionUbicacionEvent extends NoConectionEvent{
  NoConectionUbicacionEvent();
  @override
  List<Object> get props => [];
}