import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sidebar_animation/events/NavigationEvent.dart';
import 'package:sidebar_animation/model/Caza.dart';
import 'package:sidebar_animation/repository/GeoRepository.dart';
import 'package:sidebar_animation/repository/ReportsRepository.dart';
import 'package:sidebar_animation/states/exitLogin.dart';
import '../pages/reporte.dart';
import '../pages/historial.dart';
import '../pages/loadingPage.dart';

import '../pages/homepage.dart';
import '../libs/Toast.dart' as Toast;


abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvent, NavigationStates> {
  ReportsRepository reportsRepository;
  GeoRepository geoRepository;
  List<Caza> cazas=List();
  NavigationBloc(this.reportsRepository,this.geoRepository);
  @override
  NavigationStates get initialState {
    return LoadingPage("Inicio", "assets/images/imgcascada2.jpg");
  }

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvent event) async* {
    if(event is InitEvent){
      List<Caza> cazas=await reportsRepository.reportesLocal();

      yield HomePage(cazas);

    }
    else if(event is HomePageClickedEvent){
      yield LoadingPage("Inicio","assets/images/imgcascada2.jpg");
      List<Caza> cazas=await reportsRepository.reportesLocal();

      yield HomePage(cazas);
    }
    else if(event is ReporteClickedEvent){
      yield LoadingPage("Reporte","assets/images/imgreporte2.jpg");
      Position posicion=await geoRepository.obtenerUbicacion();
      yield Reporte(posicion);

    }
    else if(event is HistorialClickedEvent){
      yield LoadingPage("Historial","assets/images/imghojas.jpg");
      List cazas=await reportsRepository.obtenerReportesCaza();
      yield Historial(cazas);

    }
    else if(event is reporteClickedEvent){
      yield LoadingPage("Reporte","assets/images/imgreporte2.jpg");
      int subido=await reportsRepository.subirReporteCaza(event.caza);
      if(subido==1){
        List<Caza> cazas=await reportsRepository.reportesLocal();
        yield HomePage(cazas);
        Toast.toastOk("Reporte subido correctamente");
      }
      if(subido==2){
        yield ExitLogin();
      }
      if(subido ==0){
        List<Caza> cazas=await reportsRepository.reportesLocal();
        yield HomePage(cazas);
      }
    }
    // else if(event is informePageClickedEvent){
    //   yield InformePage();
    // }
    else if(event is reportePDFEvent) {
      Toast.toastIniciarDescarga();
      var resDescarga = await reportsRepository.obtenerReportePDF(event.id);
      if(resDescarga){
        Toast.toastOk("Descargado");
      }
      else{
        Toast.toastError("No se pudo realizar la descarga");
      }
    }
    else if(event is ReportesLocalClickedEvent) {
      yield LoadingPage("Inicio","assets/images/imgcascada2.jpg");
      int subido=await reportsRepository.subirReporteCazaLocal(event.cazas);
      if(subido==1){
        List<Caza> cazas=await reportsRepository.reportesLocal();
        yield HomePage(cazas);
        Toast.toastOk("Reporte subido correctamente");
      }
      else if(subido==2){
        yield ExitLogin();
      }
      else {
        Toast.toastError("Sin Conexion");
        List<Caza> cazas=await reportsRepository.reportesLocal();
        yield HomePage(cazas);

      }
    }
  }

}
