import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidebar_animation/model/Departamento.dart';
import 'package:sidebar_animation/model/EstadoTco.dart';
import 'package:sidebar_animation/model/Municipio.dart';
import 'package:sidebar_animation/model/Provincia.dart';
import 'package:sidebar_animation/model/Tco.dart';
import 'package:http/http.dart' as http;
import '../core/apiUrl.dart' as api;
class EstadoTcoRepository{
  Future<List<Departamento>>obtenerDepartamentos()async {
      List<Departamento> deptoList=List();
      try{
        SharedPreferences sha=await SharedPreferences.getInstance();
        String token=sha.getString("token");
        final response=await  http.get(api.url+"/departamentos",
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
              'authorizationApp':token,
              'authorization': env["API_TOKEN"]
            });
        if(response.statusCode==200){
          var deptos=jsonDecode(response.body);
          for(var depto in deptos){
            var newDepto=Departamento();
            newDepto.itemId=depto["itemId"];
            newDepto.nombreDepartamento=depto["nombre_departamento"];
            deptoList.add(newDepto);
          }
          return deptoList;
        }
        else{
          return null;
        }
    }
    catch(e){
      return null;
    }
  }
  Future<List<Provincia>>obtenerProvincias(int deptoItemId)async {
    List<Provincia> provList=List();
    try{
      SharedPreferences sha=await SharedPreferences.getInstance();
      String token=sha.getString("token");
      final response=await  http.get(api.url+"/provincias/"+deptoItemId.toString(),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'authorizationApp':token,
            'authorization': env["API_TOKEN"]
          });
      if(response.statusCode==200){
        var deptos=jsonDecode(response.body);
        for(var depto in deptos){
          var newProv=Provincia();
          newProv.itemId=depto["itemId"];
          newProv.nombreProvincia=depto["nombre_provincia"];
          provList.add(newProv);
        }
        return provList;
      }
      else{
        return null;
      }
    }
    catch(e){
      return null;
    }
  }
  Future<List<Municipio>>obtenerMunicipios(int provItemId)async {
    List<Municipio> muniList=List();
    try{
      SharedPreferences sha=await SharedPreferences.getInstance();
      String token=sha.getString("token");
      final response=await  http.get(api.url+"/municipios/"+provItemId.toString(),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'authorizationApp':token,
            'authorization': env["API_TOKEN"]
          });
      if(response.statusCode==200){
        var munis=jsonDecode(response.body);
        for(var mun in munis){
          var newMun=Municipio();
          newMun.itemId=mun["itemId"];
          newMun.nombreMunicipio=mun["nombre_municipio"];
          muniList.add(newMun);
        }
        return muniList;
      }
      else{
        return null;
      }
    }
    catch(e){
      return null;
    }
  }
  Future<List<Tco>>obtenerTcos(int muniItemId)async {
    List<Tco> tcoList=List();
    try{
      SharedPreferences sha=await SharedPreferences.getInstance();
      String token=sha.getString("token");
      final response=await  http.get(api.url+"/tcos/"+muniItemId.toString(),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'authorizationApp':token,
            'authorization': env["API_TOKEN"]
          });
      if(response.statusCode==200){
        var tcos=jsonDecode(response.body);
        for(var tco in tcos){
          var newTco=Tco();
          newTco.itemId=tco["itemId"];
          newTco.tco=tco["tco"].toString();
          tcoList.add(newTco);
        }
        return tcoList;
      }
      else{
        return null;
      }
    }
    catch(e){
      return null;
    }
  }
  Future<EstadoTco>obtenerEstado(int tcoId)async {
    try{
      SharedPreferences sha=await SharedPreferences.getInstance();
      String token=sha.getString("token");
      final response=await  http.get(api.url+"/tcos/estado/"+tcoId.toString(),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'authorizationApp':token,
            'authorization': env["API_TOKEN"]
          });
      if(response.statusCode==200){
        var estado=jsonDecode(response.body);
        EstadoTco estadoTco=EstadoTco();
        estadoTco.entregado=estado["entregado"]==null?null:int.parse(estado["entregado"].toString());
        estadoTco.tenencia=estado["custodia"]==null?null:int.parse(estado["custodia"].toString());
        estadoTco.vendido=estado["precio"]==null?null:double.parse(estado["precio"].toString());
        return estadoTco;
      }
      else{
        return null;
      }
    }
    catch(e){
      return null;
    }
  }
  Future<bool> obtenerResolucion()async {
      try {
        final response=await  http.get(api.url+"/resoluciones/"+(DateTime.now().year).toString(),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
              'authorization': env["API_TOKEN"]
            });
        if(response.statusCode==200){
          var path=jsonDecode(response.body);
          String _localPath = (await _findLocalPath()) + Platform.pathSeparator +
              'Download';
          if (await _checkPermission()) {
            File file=new File(_localPath+"/"+"resolucionMMAYA${(DateTime.now().year).toString()}.pdf");
            var exist=await file.exists();
            if(!exist){
              final response = await http.get("${api.url}/files/${path["resolucion_path"]}",
                  headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'authorization': env["API_TOKEN"]
                  });
              if (response.statusCode == 200) {
                await file.writeAsBytes(response.bodyBytes);
                OpenFile.open(_localPath+"/"+"resolucionMMAYA${(DateTime.now().year).toString()}.pdf");
                return true;
              }
              else {
                return false;
              }
            }
            else{
              OpenFile.open(_localPath+"/"+"resolucionMMAYA${(DateTime.now().year).toString()}.pdf");
              return true;

            }
          }
          else {
            return false;
          }
        }
        else{
          return false;
        }
      }
      catch (e) {
        return false;
      }
  }
  Future<String> _findLocalPath() async {
    final directory = await getExternalStorageDirectory();
    return directory.parent.parent.parent.parent.path;
  }
  Future<bool> _checkPermission() async {
    final status = await Permission.storage.status;
    if (status != PermissionStatus.granted) {
      final result = await Permission.storage.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
      else{
        return false;
      }
    } else {
      return true;
    }

  }

  }